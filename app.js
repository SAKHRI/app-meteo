'use strict';

const weatherIcons = {
    "Rain": "wi wi-day-rain",
    "Clouds": "wi wi-day-cloudy",
    "Clear": "wi wi-day-sunny",
    "Snow": "wi wi-day-snow",
    "mist": "wi wi-day-fog",
    "Drizzle": "wi wi-day-sleet",
}




function capitalize(str) {
    return str[0].toUpperCase() + str.slice(1);
}

async function main(withIP = true) {
    let ville;

    if(withIP){
    // 1.Choper l'adresse IP du PC qui ouvre la page : https://api.ipify.org?format=json

    const ip = await fetch("https://api.ipify.org?format=json")
        .then(resultat => resultat.json())
        .then(json => json.ip)

    
    // 2.A partir de l'adresse IP du PC qui ouvre la page on retourne la ville

    ville = await fetch("http://ip-api.com/json/" + ip)
        .then(resultat => resultat.json())
        .then(json => json.city)
    }else{
    ville = document.querySelector('#ville').textContent;
    }

    // 3.A partir de la ville on chope la météo

    const meteo = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${ville}&APPID=macle&lang=fr&units=metric`)
        .then(resultat => resultat.json())
        .then(json => json)
  
    // 4.Afficher les information sur la carte
    displayWeatherInfos(meteo)
    console.log(meteo);
}

function displayWeatherInfos(data){
    const name = data.name;
    const temperature = data.main.temp;
    const conditions = data.weather[0].main;
    const description = data.weather[0].description;

    document.querySelector('#ville').textContent = name;
    document.querySelector('#temperature').textContent = Math.round(temperature);
    document.querySelector('#conditions').textContent = capitalize(description);
    document.querySelector('i.wi').className = weatherIcons[conditions];

    document.body.className = conditions.toLowerCase();
}

    const ville = document.querySelector('#ville');

    ville.addEventListener('click', () => {
        ville.contentEditable = true;
    });

    ville.addEventListener('keydown', (e) => {
        if(e.keyCode === 13){
            e.preventDefault();
            ville.contentEditable = false;
            main(false)
        }
    })


main();

